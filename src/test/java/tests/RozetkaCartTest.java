package tests;

import domain.RozetkaPageSearch;
import org.testng.Assert;
import org.testng.annotations.Test;
import untils.BaseTest;

/**
 * Created by User on 25.01.2018.
 */
public class RozetkaCartTest extends BaseTest {
    private String item = "Apple iPhone 7 Plus 32GB Jet Black";

    @Test
    public void cartTest() throws InterruptedException {
        RozetkaPageSearch page = new RozetkaPageSearch(driver,wait,extent,logger);


        //click in field
        page.clickOnSearchField();
        System.out.println("click in field");

        //enter text in field
        page.enterTextInField(item);
        System.out.println("enter text in field");

        //submit entered text
        page.submitText();
        System.out.println("enter text in field");

        //assert name of product that search
        Assert.assertEquals(page.getTitleOfItem(),item);
        System.out.println("assert name of product that search");

        //wait for button "Buy" and click on button
        page.clickAndBuy();
        System.out.println("wait for button \"Buy\" and click on button");

        //wait for popup window
        page.waitForPopupWhenBuy();
        System.out.println("wait for popup window");

        //go into popup window
        page.goToPopUpWindow();
        System.out.println("go into popup window");

        //assert name of buying product in
        Assert.assertTrue(page.getNameInOrder().contains(item));
        System.out.println("assert name of buying product in");

        //click on close button
        page.clickOnClose();
        System.out.println("click on close button");

        //assert name of buying product in stack
        Assert.assertEquals(page.getTitleOfItem(),item);
        System.out.println("assert name of buying product in stack");

        // open cart
        page.openBasket();
        System.out.println("open cart");

        //go to popup window
        page.goToPopUpWindow();
        System.out.println("go to popup window");

        //assert name in order
        Assert.assertEquals(page.getNameInOrder(),item);
        System.out.println("assert name in order");

        //try to remove from order
        page.tryToRemoveFromCart();
        System.out.println("try to remove from order");

        //remove from cart
        page.removeFromCart();
        System.out.println("remove from cart");

        //assert item order is empty
        Assert.assertTrue(page.getEmptyOrder().contains("пуста"));
        System.out.println("assert item order is empty");

        //click on close button
        page.clickOnClose();
        System.out.println("click on close button");

        //assert name of product that search
        Assert.assertEquals(page.getTitleOfItem(),item);
        System.out.println("assert name of product that search");

        //refresh window
        page.refreshWindow(driver);
        System.out.println("refresh window");

        //assert name of product that search
        Assert.assertEquals(page.getTitleOfItem(),item);
        System.out.println("assert name of product that search");

        // open cart
        page.openBasket();
        System.out.println("open cart");

        //go to popup window
        page.goToPopUpWindow();
        System.out.println("go to popup window");

        //assert item order is empty
        Assert.assertTrue(!page.getEmptyOrder().isEmpty());
        System.out.println("assert item order is empty");

        //click on close button
        page.clickOnClose();
        System.out.println("click on close button");
}

    @Test
    public void cartTest2() throws InterruptedException {
        RozetkaPageSearch page = new RozetkaPageSearch(driver,wait,extent,logger);


        //click in field
        page.clickOnSearchField();
        System.out.println("click in field");

        //enter text in field
        page.enterTextInField(item);
        System.out.println("enter text in field");

        //submit entered text
        page.submitText();
        System.out.println("enter text in field");

        //assert name of product that search
        Assert.assertEquals(page.getTitleOfItem(),item);
        System.out.println("assert name of product that search");

        //wait for button "Buy" and click on button
        page.clickAndBuy();
        System.out.println("wait for button \"Buy\" and click on button");

        //wait for popup window
        page.waitForPopupWhenBuy();
        System.out.println("wait for popup window");

        //go into popup window
        page.goToPopUpWindow();
        System.out.println("go into popup window");

        //assert name of buying product in
        Assert.assertTrue(page.getNameInOrder().contains(item));
        System.out.println("assert name of buying product in");

        //click on close button
        page.clickOnClose();
        System.out.println("click on close button");

        //assert name of buying product in stack
        Assert.assertEquals(page.getTitleOfItem(),item);
        System.out.println("assert name of buying product in stack");

        // open cart
        page.openBasket();
        System.out.println("open cart");

        //go to popup window
        page.goToPopUpWindow();
        System.out.println("go to popup window");

        //assert name in order
        Assert.assertEquals(page.getNameInOrder(),item);
        System.out.println("assert name in order");

        //try to remove from order
        page.tryToRemoveFromCart();
        System.out.println("try to remove from order");

        //remove from cart
        page.removeFromCart();
        System.out.println("remove from cart");

        //assert item order is empty
        Assert.assertTrue(page.getEmptyOrder().contains("пуста"));
        System.out.println("assert item order is empty");

        //click on close button
        page.clickOnClose();
        System.out.println("click on close button");

        //assert name of product that search
        Assert.assertEquals(page.getTitleOfItem(),item);
        System.out.println("assert name of product that search");

        //refresh window
        page.refreshWindow(driver);
        System.out.println("refresh window");

        //assert name of product that search
        Assert.assertEquals(page.getTitleOfItem(),item);
        System.out.println("assert name of product that search");

        // open cart
        page.openBasket();
        System.out.println("open cart");

        //go to popup window
        page.goToPopUpWindow();
        System.out.println("go to popup window");

        //assert item order is empty
        Assert.assertTrue(false);
        System.out.println("assert item order is empty");

        //click on close button
        page.clickOnClose();
        System.out.println("click on close button");
    }


}
