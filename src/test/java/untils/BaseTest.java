package untils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by User on 25.01.2018.
 */
public class BaseTest {
    public WebDriver driver;
    public WebDriverWait wait;
    public ChromeDriverService service;
    public static ExtentReports extent;
    public static ExtentTest logger;


    @BeforeTest
    public void beforeTest() throws IOException {
        extent = new ExtentReports("target/reports/Report.html");
        extent.addSystemInfo("Host Name", "Test PC");
        extent.addSystemInfo("Env", "Automation Testing");
        extent.addSystemInfo("User Name", "Andrii Fedosov");

        extent.loadConfig(new File("src/main/resources/config/extent-config.xml"));
    }

    @BeforeMethod
    public void setUp() {
        logger = extent.startTest(getClass().getName());
        logger.log(LogStatus.INFO, "Try to create driver.");
        try {
            service = new ChromeDriverService.Builder()
                    .usingDriverExecutable(new File("src/main/resources/config/chromedriver.exe"))
                    .usingAnyFreePort()
                    .build();
            service.start();
//            System.setProperty("webdriver.chrome.driver","src/main/resources/config/chromedriver.exe");
            driver = new RemoteWebDriver(service.getUrl(),
                    DesiredCapabilities.chrome());
            driver.manage().window().maximize();
            driver.get("https://rozetka.com.ua/");
            wait = new WebDriverWait(driver, 10);
            logger.log(LogStatus.PASS, "Driver create");

        } catch (Exception e) {
            logger.log(LogStatus.ERROR, "Can`t create driver");
        }
    }
    public static String getScreenhot(WebDriver driver, String screenshotName) throws Exception {
        String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        //after execution, you could see a folder "FailedTestsScreenshots" under src folder
        String destination = "/main/resources/FailedTestsScreenshots/"+screenshotName+"_"+dateName+".png";
        File finalDestination = new File(destination);
        FileUtils.copyFile(source,finalDestination);
        return destination;
    }

    @AfterMethod
    public void shutTest(ITestResult result) throws Exception {
        try {
            logger.log(LogStatus.PASS,"Try to close driver");
            if(result.getStatus() == ITestResult.FAILURE){
                logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
                logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
                String screenshotPath = BaseTest.getScreenhot(driver,result.getName());
                logger.log(LogStatus.FAIL, logger.addBase64ScreenShot(screenshotPath));
            }else if(result.getStatus() == ITestResult.SKIP){
                logger.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
            }
            logger.log(LogStatus.PASS,"Driver was closed");
        }catch (Exception e){
            logger.log(LogStatus.FAIL,"We cant close driver");
        }finally {
            driver.quit();
            service.stop();
            extent.flush();
        }
        extent.endTest(logger);
        // ending test
        //endTest(logger) : It ends the current test and prepares to create HTML report

    }

    @AfterTest
    public void afterTest() {

        extent.close();
    }
}