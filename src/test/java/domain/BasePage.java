package domain;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;

/**
 * Created by User on 29.01.2018.
 */
public class BasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private ExtentReports reports;
    private ExtentTest logger;

    public BasePage(WebDriver driver, WebDriverWait wait, ExtentReports reports, ExtentTest logger) {
        this.driver = driver;
        this.wait = wait;
        this.reports = reports;
        this.logger = logger;
    }

    public void waitForElement(WebElement element){
        logger.log(LogStatus.INFO,"You wait for element");
        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            logger.log(LogStatus.PASS,"Element  is visible");
        }catch (Exception e){
            logger.log(LogStatus.ERROR,"Cant wait for element. Error is :" + Arrays.toString(e.getStackTrace()));
        }
    }

    public void click(WebElement element){
        logger.log(LogStatus.INFO,"You try click on element" );
        try{
            element.click();
            System.out.println("Click on element");
            logger.log(LogStatus.PASS,"You clicked on element " );
        }catch (Exception e){
            logger.log(LogStatus.ERROR,"You cant click on element. Error is :" + Arrays.toString(e.getStackTrace()));
        }
    }

    public void enterText(WebElement element, String text){
        logger.log(LogStatus.INFO,"Try to enter text ");
        try{
            element.sendKeys(text);
            System.out.println("Search");
            logger.log(LogStatus.PASS,"You entered text");
        }catch (Exception e) {
            logger.log(LogStatus.ERROR,"Text not entered. Error is :" + Arrays.toString(e.getStackTrace()));
        }
    }

    public void submitItem(WebElement element){
        logger.log(LogStatus.INFO,"Try to submit");
        try{
            element.submit();
            System.out.println("Submit");
            logger.log(LogStatus.PASS,"You send form");
        }catch (Exception e) {
            logger.log(LogStatus.ERROR,"Form  not submit . Error is :" + e.getMessage());
        }
    }

    public void clickOnButton(WebElement element){
        logger.log(LogStatus.INFO,"You wait for button ");
        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            logger.log(LogStatus.PASS,"Button is visible");
        }catch (Exception e){
            logger.log(LogStatus.ERROR,"Button is not visible .");
        }
        logger.log(LogStatus.INFO,"Try to click on button");
        try{
            element.click();
            Thread.sleep(2000);
            System.out.println("Click on button");
            logger.log(LogStatus.PASS,"Button is clicked");
        }catch (Exception e){
            logger.log(LogStatus.ERROR,"Cant click on button .Error is :" + Arrays.toString(e.getStackTrace()));
        }
    }

    public String getProductTitle(WebElement element){
        logger.log(LogStatus.INFO,"Try assert name of item");
        try{
            System.out.println("Look at the name of item");
            logger.log(LogStatus.PASS,"Title is : " + element.getText() );
            return element.getText().trim().replace("&nbsp;","");
        }catch (Exception e){
            
            logger.log(LogStatus.ERROR,"Title is wrong .Error is :" + Arrays.toString(e.getStackTrace()));
        }
        return null;
    }

    public void waitForPopup(WebElement element){
        logger.log(LogStatus.INFO,"Try to open new window ");
        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            System.out.println("Wait for new window");
            logger.log(LogStatus.PASS,"Window is visible");
        }catch (Exception e){
            logger.log(LogStatus.ERROR,"Cant open the window .Error is :" + Arrays.toString(e.getStackTrace()));
        }
    }

    public void goIntoWindow(){

        logger.log(LogStatus.INFO,"Try to create parent window ");
        try{
            String hendler = driver.getWindowHandle();
            driver.switchTo().window(hendler);
            System.out.println("Go to pop up window");
            logger.log(LogStatus.PASS,"Window is created");
        }catch (Exception e){
            logger.log(LogStatus.ERROR,"Cant open the window .Error is :" + Arrays.toString(e.getStackTrace()));
        }
     }

     public void refresh(WebDriver driver){

         logger.log(LogStatus.INFO,"Try to refresh window ");
         try{
             driver.navigate().refresh();
             System.out.println("Wait for  window");
             logger.log(LogStatus.PASS,"Window is refreshed");
         }catch (Exception e){
             e.printStackTrace();
             logger.log(LogStatus.ERROR,"Cant refresh window .Error is :" + Arrays.toString(e.getStackTrace()));
         }
     }


}
