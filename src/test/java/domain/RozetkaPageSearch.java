package domain;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by User on 25.01.2018.
 */
public class RozetkaPageSearch  extends BasePage{
    private WebDriver driver;
    private WebDriverWait wait;
    private  ExtentReports reports;
    private ExtentTest logger;


    @FindBy(className = "rz-header-search-input-text")
    private WebElement searchField;

    @FindBy(xpath = "//h1")
    private WebElement nameOfItem;

    @FindBy(name = "topurchases")
    private WebElement buyButton;

    @FindBy(id = "cart_payment_info")
    private WebElement popup;

    @FindBy(xpath = "//img[@alt='×']")
    private WebElement goNextButton;

    @FindBy(xpath = "//a[@name='goods-link' and @class]")
    private WebElement itemInCartName;

    @FindBy(xpath = "//div[contains(@id,'cart_block')]")
    private WebElement basketButton;

    @FindBy(name = "before_delete")
    private  WebElement removeButton;

    @FindBy(name = "delete")
    private WebElement agreeRemoveButton;

    @FindBy(xpath = "//h2[contains(@class,'empty-cart-title')]")
    private WebElement textOfEmptyBasket;

    @FindBy(xpath = "//img[contains(@class,'popup-close-icon')]")
    private  WebElement closeWindowButton;


    public RozetkaPageSearch(WebDriver driver, WebDriverWait wait, ExtentReports reports, ExtentTest logger) {
        super(driver, wait, reports, logger);
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public  void clickOnSearchField(){
        waitForElement(searchField);
        click(searchField);
    }
    public void enterTextInField(String text){
        enterText(searchField,text);
    }

    public void submitText() {
        submitItem(searchField);
    }

    public String getTitleOfItem(){
        return getProductTitle(nameOfItem);
    }

    public void clickAndBuy(){
        clickOnButton(buyButton);
    }


    public void waitForPopupWhenBuy(){
        waitForPopup(popup);
    }

    public void goToPopUpWindow(){
        goIntoWindow();
    }

    public void clickOnClose() {
        waitForElement(closeWindowButton);
        clickOnButton(closeWindowButton);
    }


    public String getNameInOrder() {
        waitForElement(itemInCartName);
        return itemInCartName.getText() ;
    }

    public void openBasket() throws InterruptedException {
            waitForElement(basketButton);
            click(basketButton);
    }

    public void tryToRemoveFromCart(){
        waitForElement(removeButton);
        click(removeButton);
    }

    public void removeFromCart(){
        waitForElement(agreeRemoveButton);
        click(agreeRemoveButton);
    }

    public String getEmptyOrder(){
        waitForElement(textOfEmptyBasket);
        return textOfEmptyBasket.getText() ;
    }

    public void refreshWindow(WebDriver driver){
        refresh(driver);
    }



}
